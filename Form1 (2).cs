﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Text.RegularExpressions;
using ZedGraph;
using System.IO;

namespace ase_cyclingapp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string filename;
        public Form1(string filename)
        {
            InitializeComponent();
            this.filename = filename;
        }

        List<double> hr = new List<double>();
        List<double> spd = new List<double>();
        List<double> alt = new List<double>();
        List<double> pwr = new List<double>();
        List<double> cad = new List<double>();

        string version1, monitor1, date1, time1, weight1, smode1, interval1, length1;
        static string[] getParam(string line)
        {
            return line.Split('=');
        }
        static string[] getData(string line)
        {
            return Regex.Split(line, @"\W+");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                bool isParameter = false;
                bool isData = false;
                using (StreamReader sr = new StreamReader(filename))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (isParameter)
                        {
                            if (line.Length > 0)
                            {
                                string[] p = getParam(line);
                                if (p[1] != null)
                                {
                                    switch (p[0])
                                    {
                                        case "Version":
                                            version1 = p[1];
                                            version.Text = "Version    " + p[1];
                                            break;
                                        case "Monitor":
                                            monitor1 = p[1];
                                            monitor.Text = "Monitor    " + p[1];
                                            break;
                                        case "SMode":
                                            smode1 = p[1];
                                            smode.Text = "SMode    " + p[1];
                                            break;
                                        case "Date":
                                            date1 = p[1];
                                            date.Text = "Date    " + p[1];
                                            break;
                                        case "StartTime":
                                            time1 = p[1];
                                            time.Text = "StartTime    " + p[1];
                                            break;
                                        case "Weight":
                                            weight1 = p[1];
                                            weight.Text = "Weight    " + p[1];
                                            break;
                                        case "Length":
                                            length1 = p[1];
                                            length.Text = "Length    " + p[1];
                                            break;
                                        case "Interval":
                                            interval1 = p[1];
                                            interval.Text = "Interval    " + p[1];
                                            break;

                                    }
                                }
                            }
                        }
                        if (line.Length < 1 || line.Substring(0, 1) == "[")
                        {
                            isParameter = false;
                        }
                        if (line == "[Params]")
                        {
                            isParameter = true;
                        }
                        if (isData)
                        {
                            string[] d = getData(line);

                            hr.Add(Convert.ToDouble(d[0]));
                            spd.Add(Convert.ToDouble(d[1]));
                            cad.Add(Convert.ToDouble(d[2]));
                            alt.Add(Convert.ToDouble(d[3]));
                            pwr.Add(Convert.ToDouble(d[4]));
                        }
                        if (line == "[HRData]")
                        {
                            isData = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("the file could not be read:");
                Console.WriteLine(ex.ToString());
            }
            double distance1 = 0;
            for (int i = 0; i < spd.Count; i++)
            {
                distance1 += (spd[i] / 10) / 3600;
            }
            distance.Text = "Total Distance    " + distance1 + " km";

            maxhr.Text = "Max HR    " + hr.Max().ToString();
            minhr.Text = "Min HR    " + hr.Min().ToString();
            avghr.Text = "Avg HR    " + hr.Average().ToString();

            maxspd.Text = "Max Speed    " + spd.Max().ToString() + " km/h";
            avgspd.Text = "Avg Speed    " + spd.Average().ToString() + " km/h";

            maxcad.Text = "Max Cadence    " + cad.Max().ToString();
            avgcad.Text = "Avg Cadence    " + cad.Average().ToString();

            maxalt.Text = "Max Altitude    " + alt.Max().ToString();
            avgalt.Text = "Avg Altitude    " + alt.Average().ToString();

            maxpwr.Text = "Max Power    " + pwr.Max().ToString();
            avgpwr.Text = "Avg Power    " + pwr.Average().ToString();

            double min = 0.0167, pwr4, pwrT;
            double pwrTotal = 0;
            for (int i = 0; i < pwr.Count; i++)
            {
                pwr4 = Math.Pow(pwr[i], 4);
                pwrT = min * pwr4;
                pwrTotal += pwrT;
            }

            double np = Math.Pow((pwrTotal / 60), 1/4);
            normalised.Text = "Normalised Power     " + np;

            double ap = pwr.Average();
            double ftp = ap - ((5 / 100) * ap);
            double intF = np / ftp;
            intensity.Text = "Intensity Factor    " + intF;

            double tss = (pwr.Count * np * intF) / (ftp * 3600) * 100;
            stress.Text = "Total Stress Score     " + tss;



            dataGridView1.Columns.Add("HR", "HR");
            dataGridView1.Columns.Add("SPEED", "SPEED");
            dataGridView1.Columns.Add("CADENCE", "CADENCE");
            dataGridView1.Columns.Add("ALTITUDE", "ALTITUDE");
            dataGridView1.Columns.Add("POWER", "POWER");

            for (int i = 0; i < hr.Count; i++)
            {
                dataGridView1.Rows.Add(hr[i], spd[i], cad[i], alt[i], pwr[i]);
            }
            zgc = zedGraphControl1;
            myPane = zgc.GraphPane;

            myPane.XAxis.Scale.Min = 0;
            myPane.XAxis.Scale.Max = hr.Count;

            zgc.ScrollMinX = 0;
            zgc.ScrollMaxX = hr.Count;

            myPane.YAxis.Title.Text = "hr";
            myPane.Y2Axis.Title.Text = "Speed";
            myPane.Y2Axis.IsVisible = true;


            var Y3Axis = myPane.AddYAxis("Cadence");
            var Y4Axis = myPane.AddYAxis("Altitude");
            var Y5Axis = myPane.AddYAxis("Power");

            double x, y1, y2, y3, y4, y5;

            for (int i = 0; i < hr.Count; i++)
            {
                x = i;
                y1 = hr[i];
                y2 = spd[i];
                y3 = cad[i];
                y4 = alt[i];
                y5 = pwr[i];

                hrList.Add(x, y1);
                spdList.Add(x, y2);
                cadList.Add(x, y3);
                altList.Add(x, y4);
                pwrList.Add(x, y5);
            }

            myCurve1 = myPane.AddCurve("Heart Rate", hrList, Color.Red, SymbolType.None);
            myCurve2 = myPane.AddCurve("Speed", spdList, Color.Blue, SymbolType.None);
            myCurve2.IsY2Axis = true;
            myCurve4 = myPane.AddCurve("Cadence", altList, Color.Green, SymbolType.None);
            myCurve4.YAxisIndex = Y3Axis;
            myCurve3 = myPane.AddCurve("Altitude", cadList, Color.Cyan, SymbolType.None);
            myCurve4.YAxisIndex = Y4Axis;
            myCurve5 = myPane.AddCurve("Power", pwrList, Color.Pink, SymbolType.None);
            myCurve4.YAxisIndex = Y5Axis;

            zgc.AxisChange();
            zgc.Invalidate();

        }

        ZedGraphControl zgc;
        GraphPane myPane;
        PointPairList hrList = new PointPairList();
        PointPairList spdList = new PointPairList();
        PointPairList cadList = new PointPairList();
        PointPairList altList = new PointPairList();
        PointPairList pwrList = new PointPairList();

        LineItem myCurve1, myCurve2, myCurve3, myCurve4, myCurve5;

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                myCurve1.IsVisible = true;
                zgc.Invalidate();
            }
            else
            {
                myCurve1.IsVisible = false;
                zgc.Invalidate();
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                myCurve2.IsVisible = true;
                zgc.Invalidate();
            }
            else
            {
                myCurve2.IsVisible = false;
                zgc.Invalidate();
            }

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked)
            {
                myCurve3.IsVisible = true;
                zgc.Invalidate();
            }
            else
            {
                myCurve3.IsVisible = false;
                zgc.Invalidate();
            }
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {

            if (checkBox4.Checked)
            {
                myCurve4.IsVisible = true;
                zgc.Invalidate();
            }
            else
            {
                myCurve4.IsVisible = false;
                zgc.Invalidate();
            }
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {

            if (checkBox5.Checked)
            {
                myCurve5.IsVisible = true;
                zgc.Invalidate();
            }
            else
            {
                myCurve5.IsVisible = false;
                zgc.Invalidate();
            }
        }



        private void dataGridView1_MouseUp(object sender, MouseEventArgs e)
        {
            List<double> hr1 = new List<double>();
            List<double> spd1 = new List<double>();
            List<double> alt1 = new List<double>();
            List<double> pwr1 = new List<double>();
            List<double> cad1 = new List<double>();

            foreach (DataGridViewRow dgvr in dataGridView1.SelectedRows)
            {
                hr1.Add(Convert.ToInt16(dgvr.Cells[0].Value));
                spd1.Add(Convert.ToInt16(dgvr.Cells[1].Value));
                cad1.Add(Convert.ToInt16(dgvr.Cells[2].Value));
                alt1.Add(Convert.ToInt16(dgvr.Cells[3].Value));
                pwr1.Add(Convert.ToInt16(dgvr.Cells[4].Value));
            }

            Form graph = new Form2(hr1, spd1, alt1, pwr1, cad1);
            graph.Show();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace ase_cyclingapp
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        List<double> hr = new List<double>();
        List<double> spd = new List<double>();
        List<double> alt = new List<double>();
        List<double> pwr = new List<double>();
        List<double> cad = new List<double>();

        public Form2(List<double> hr, List<double> spd, List<double> alt, List<double> pwr, List<double> cad)
        {
            InitializeComponent();
            this.hr = hr;
            this.spd = spd;
            this.alt = alt;
            this.pwr = pwr;
            this.cad = cad;
        }


        ZedGraphControl zgc;
        GraphPane myPane;
        PointPairList hrList = new PointPairList();
        PointPairList spdList = new PointPairList();
        PointPairList cadList = new PointPairList();
        PointPairList altList = new PointPairList();
        PointPairList pwrList = new PointPairList();

        LineItem myCurve1, myCurve2, myCurve3, myCurve4, myCurve5;
        private void Form2_Load(object sender, EventArgs e)
        {

            zgc = zedGraphControl1;
            myPane = zgc.GraphPane;

            myPane.XAxis.Scale.Min = 0;
            myPane.XAxis.Scale.Max = hr.Count;

            zgc.ScrollMinX = 0;
            zgc.ScrollMaxX = hr.Count;

            myPane.YAxis.Title.Text = "hr";
            myPane.Y2Axis.Title.Text = "Speed";
            myPane.Y2Axis.IsVisible = true;


            var Y3Axis = myPane.AddYAxis("Cadence");
            var Y4Axis = myPane.AddYAxis("Altitude");
            var Y5Axis = myPane.AddYAxis("Power");

            double x, y1, y2, y3, y4, y5;

            for (int i = 0; i < hr.Count; i++)
            {
                x = i;
                y1 = hr[i];
                y2 = spd[i];
                y3 = cad[i];
                y4 = alt[i];
                y5 = pwr[i];

                hrList.Add(x, y1);
                spdList.Add(x, y2);
                cadList.Add(x, y3);
                altList.Add(x, y4);
                pwrList.Add(x, y5);
            }

            myCurve1 = myPane.AddCurve("Heart Rate", hrList, Color.Red, SymbolType.None);
            myCurve2 = myPane.AddCurve("Speed", spdList, Color.Blue, SymbolType.None);
            myCurve2.IsY2Axis = true;
            myCurve4 = myPane.AddCurve("Cadence", altList, Color.Green, SymbolType.None);
            myCurve4.YAxisIndex = Y3Axis;
            myCurve3 = myPane.AddCurve("Altitude", cadList, Color.Cyan, SymbolType.None);
            myCurve4.YAxisIndex = Y4Axis;
            myCurve5 = myPane.AddCurve("Power", pwrList, Color.Pink, SymbolType.None);
            myCurve4.YAxisIndex = Y5Axis;

            zgc.AxisChange();
            zgc.Invalidate();

            label1.Text = "Average HR: " + hr.Average();
            label2.Text = "Average Speed: " + (spd.Average() / 10);
            label3.Text = "Average Altitude: " + alt.Average();
            label4.Text = "Average Power: " + pwr.Average();
            label5.Text = "Average Cadence: " + cad.Average();
        }
    }
}
